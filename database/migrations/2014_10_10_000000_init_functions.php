<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitFunctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $function = [
            'UCID' =>   "CREATE FUNCTION UCID() RETURNS char(16) CHARSET ascii
                        BEGIN
                            SET @output = TO_BASE64(random_bytes(12));
                            return @output;
                        END",
            'ULID' =>   "CREATE FUNCTION ULID(string TEXT CHARSET utf8mb4) RETURNS char(64) CHARSET ascii
                        BEGIN
                            set @string = SHA1(string);
                            set @sha384 = SHA2(UNHEX(@string), 384);
                            set @base64 = TO_BASE64(UNHEX(@sha384));
                            set @output = url_base64_encode(@base64);
                            return @output;
                        END",
            'URID' =>   "CREATE FUNCTION URID() RETURNS char(64) CHARSET latin1
                        BEGIN
                            set @random = concat(RANDOM_BYTES(48));
                            set @sha384 = SHA2(@random, 384);
                            set @output = TO_BASE64(UNHEX(@sha384));
                            RETURN @output;
                        END",
            'USID' =>   "CREATE FUNCTION USID(string TEXT, pass TEXT) RETURNS text CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci
                        BEGIN
                            SET @string = encode(string,unhex(sha1(pass)));
                            set @base64 = TO_BASE64(@string);
                            set @output = url_base64_encode(@base64);
                            return @base64;
                        END",
            'url_base64_encode' =>  "CREATE FUNCTION url_base64_decode(string TEXT) RETURNS text CHARSET utf8 COLLATE utf8_unicode_ci RETURN REPLACE(REPLACE(string,'-','+'),'_','+')",
            'url_base64_decode' =>  "CREATE FUNCTION url_base64_encode(string TEXT CHARSET utf8mb4) RETURNS text CHARSET ascii RETURN REPLACE(REPLACE(string,'+','-'),'/','_')",
            'randnum' =>    "CREATE FUNCTION randnum(imin INT, imax INT) RETURNS bigint(20) unsigned
                            BEGIN
                                RETURN FLOOR(RAND()*(imax-imin+1)+imin);
                            END",
            'randstr' =>    "CREATE FUNCTION randstr(length SMALLINT(4) UNSIGNED) RETURNS text CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci
                            BEGIN
                                SET @int = length;
                                SET @string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';

                                SET @min = 1;
                                SET @max = 65;
                                SET @base64 = '';
                                SET @i = 0;

                                WHILE(length(@base64) < @int) DO
                                    SET @randnum = FLOOR(RAND() * 64)+1;
                                    SET @char = SUBSTR(@string, @randnum, 1);
                                    SET @base64 = concat(@char,@base64);
                                    SET @i = @i + 1;
                                END WHILE;

                                #RETURN lpad(@base64,11,0);
                                RETURN @base64;
                            END",
            'str_shuffle' =>    "CREATE FUNCTION str_shuffle(v_chars TEXT) RETURNS text CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci
                                BEGIN DECLARE v_retval TEXT DEFAULT '';
                                    DECLARE u_pos    INT UNSIGNED;
                                    DECLARE u        INT UNSIGNED;
                                    SET u = LENGTH(v_chars);
                                    WHILE u > 0 DO SET u_pos = 1 + FLOOR(RAND() * u);
                                    SET v_retval = CONCAT(v_retval, MID(v_chars, u_pos, 1));
                                    SET v_chars = CONCAT(LEFT(v_chars, u_pos - 1), MID(v_chars, u_pos + 1, u));
                                    SET u = u - 1;
                                    END WHILE;
                                    RETURN v_retval;
                                END",
        ];

    public function up()
    {
        $query = $this->function;

        foreach($query as $query_key => $query_value) {
            DB::unprepared($query_value);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $query = $this->function;

        foreach($query as $query_key => $query_value) {
            DB::unprepared('DROP FUNCTION IF EXISTS '.$query_key.';');
        }
    }

    // function query(String $query){
    //     return 'DELIMITER $$'.$query.'$$DELIMITER ;';
    // }
}
