<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->char('uid', 16)->unique();
            $table->string('name', 64);
            $table->string('username', 36)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 128);
            $table->rememberToken();
            $table->timestamp('last_logged_in_at')->nullable();
            $table->string('picture')->nullable();
            $table->boolean('enabled')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['email', 'deleted_at']);
        });

        DB::unprepared('CREATE TRIGGER `bi_users`
            BEFORE INSERT ON `users`
            FOR EACH ROW
            BEGIN
                SET @str = unhex(\'E38080E38080E38080\');
                SET new.uid = USID(@str,new.username);
            END
            ');
        DB::statement("ALTER TABLE `users` CHANGE `uid` `uid` CHAR(16) CHARACTER SET ascii COLLATE ascii_bin NOT NULL");

        // // Create table for associating roles to users (Many To Many Polymorphic)
        // Schema::create('user_roles', function (Blueprint $table) {
        //     $table->integer('user_id')->unsigned();
        //     $table->integer('role_id')->unsigned();
        //     $table->string('user_type');

        //     $table->foreign('role_id')->references('id')->on('roles')
        //         ->onDelete('cascade')->onUpdate('cascade');

        //     $table->foreign('user_id')->references('id')->on('users')
        //         ->onDelete('cascade')->onUpdate('cascade');

        //     $table->primary(['user_id', 'role_id']);
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('user_roles');
        Schema::dropIfExists('users');
    }
}


