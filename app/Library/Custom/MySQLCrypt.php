<?php
namespace App\Library\Custom;

class rand {
  private $_seed1;
  private $_seed2;
  private $_max_value;

  public function __construct($seed1,$seed2) {
    $this->_max_value = 1073741823; // x3fffffff
    $this->_seed1 = (int)((float)$seed1 % $this->_max_value);
    $this->_seed2 = (int)((float)$seed2 % $this->_max_value);
  } // end-function __construct

  public function my_rnd() {
    $this->_seed1 = intval(fmod((($this->_seed1 * 3) + $this->_seed2),$this->_max_value));
    $this->_seed2 = intval(fmod(($this->_seed1 + $this->_seed2 + 33),$this->_max_value));
    return (float)$this->_seed1 / $this->_max_value;
  } // end-function my_rnd
} // end-class rand

class MySQLCrypt {
  private $_decode_buff;
  private $_encode_buff;
  private $_rand;
  private $_shift;
  private $_rand_org;

  public function __construct($password='') {
    $rand_nr = $this->_hash_password($password);
    $this->_crypt_init($rand_nr);
  } // end-function __construct

  private function _crypt_init($rand_nr) {
    $this->_rand = new rand($rand_nr[0],$rand_nr[1]);

    for ($i=0; $i<=255; $i++)
      $this->_decode_buff[$i] = chr($i);

    for ($i=0; $i<=255; $i++) {
      $idx = intval($this->_rand->my_rnd() * 255.0);
      $a = $this->_decode_buff[$idx];
      $this->_decode_buff[$idx] = $this->_decode_buff[$i];
      $this->_decode_buff[$i] = $a;
    } // end-for

    for ($i=0; $i<=255; $i++)
      $this->_encode_buff[ord($this->_decode_buff[$i])] = chr($i);

    $this->_shift = 0;
    $this->_rand_org = clone $this->_rand;
  } // end-function _crypt_init

  private function _hash_password($password) {
    $nr = 1345345333;
    $nr2 = 305419889;
    $add = 7;

    $result = array();
    for ($i=0; $i<strlen($password); $i++) {
      if ($password[$i] == ' ' or $password[$i] == "\t") {
      } else {
        $tmp = ord($password[$i]);

        $nr ^= ((($nr & 63) + $add) * $tmp) + ($nr * 256);
        $nr = $this->_makeInt($nr);
        $nr2 += ($nr2 * 256) ^ $nr;
        $nr2 = $this->_makeInt($nr2);

        $add += $tmp;
      }
    } // end-for
    $result[0] = $nr & 2147483647; // 7fffffff
    $result[1] = $nr2 & 2147483647; // 7fffffff
    return $result;
  } // end-function _hash_password

  private function _makeInt($value) {
    if ($value >= 2147483648)
      $result = intval($value - 2147483648);
    else if ($value < 0)
      $result = intval($value + 2147483648);
    else
      $result = $value;
    return $result;
  } // end-function _makeInt

  public function decode($str,$password=null) {
    if ($password === null) {
      $this->_shift = 0;
      $this->_rand = clone $this->_rand_org;
    } else
      $this->__construct($password);

    $c_str = $str;

    for ($i=0; $i<strlen($str); $i++) {
      $this->_shift ^= intval($this->_rand->my_rnd() * 255.0);
      $idx = ord($c_str[$i]) ^ $this->_shift;
      $c_str[$i] = $this->_decode_buff[$idx];
      $this->_shift ^= ord($c_str[$i]);
    } // end-for
    return $c_str;
  } // end-function decode

  public function encode($str,$password=null) {
    if ($password === null) {
      $this->_shift = 0;
      $this->_rand = clone $this->_rand_org;
    } else
      $this->__construct($password);

    $c_str = $str;

    for ($i=0; $i<strlen($str); $i++) {
      $this->_shift ^= intval($this->_rand->my_rnd() * 255.0);
      $idx = ord($c_str[$i]);
      $c_str[$i] = chr(ord($this->_encode_buff[$idx]) ^ $this->_shift);
      $this->_shift ^= $idx;
    } // end-for
    return $c_str;
  } // end-function encode
} // end-class MySQLCrypt